﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace house_sit_me_website.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<SitterAccount> SitterAccounts { get; set; }
        public DbSet<OwnerAccount> OwnerAccounts { get; set; }
        public DbSet<HouseSearch> HouseSearches { get; set; }
        public DbSet<UploadHouse> UploadHouses { get; set; }
    }
}
