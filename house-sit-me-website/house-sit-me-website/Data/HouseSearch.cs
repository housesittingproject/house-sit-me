﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace house_sit_me_website.Data
{
    public class HouseSearch
    {
        public int ID { get; set; }
        public string HouseType { get; set; }
        public string AreaType { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string State { get; set; } 
        public int Zip { get; set; }
        public int Dates { get; set; }
        public int Radius { get; set; } 
        public string Transportation { get; set; }
        public string PaymentType { get; set; }
        public string HavePlants { get; set; }
        public string Plants { get; set; }
        public int NumberOfPlants { get; set; }
        public string HavePets { get; set; }
        public string Pets { get; set; }
        public int NumberOfPets { get; set; }
        
    }
}
