﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace house_sit_me_website.Data.Migrations
{
    public partial class SitterAccount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "BirthDate",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedAt",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Password",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "OwnerAccounts",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    BirthDate = table.Column<DateTime>(nullable: false),
                    PasswordConfirmed = table.Column<string>(nullable: true),
                    EmailConfirmed = table.Column<string>(nullable: true),
                    CriminalBackgroundAgreement = table.Column<string>(nullable: true),
                    AssignedToId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OwnerAccounts", x => x.ID);
                    table.ForeignKey(
                        name: "FK_OwnerAccounts_AspNetUsers_AssignedToId",
                        column: x => x.AssignedToId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SitterAccounts",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    BirthDate = table.Column<DateTime>(nullable: false),
                    PasswordConfirmed = table.Column<string>(nullable: true),
                    EmailConfirmed = table.Column<string>(nullable: true),
                    CriminalBackgroundAgreement = table.Column<string>(nullable: true),
                    AssignedToId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SitterAccounts", x => x.ID);
                    table.ForeignKey(
                        name: "FK_SitterAccounts_AspNetUsers_AssignedToId",
                        column: x => x.AssignedToId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UploadHouses",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    HouseType = table.Column<string>(nullable: true),
                    AreaType = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    Zip = table.Column<int>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    Radius = table.Column<int>(nullable: false),
                    Transportation = table.Column<string>(nullable: true),
                    PaymentType = table.Column<string>(nullable: true),
                    HavePlants = table.Column<string>(nullable: true),
                    Plants = table.Column<string>(nullable: true),
                    NumberOfPlants = table.Column<int>(nullable: false),
                    HavePets = table.Column<string>(nullable: true),
                    Pets = table.Column<string>(nullable: true),
                    NumberOfPets = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    OwnerAccountID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UploadHouses", x => x.ID);
                    table.ForeignKey(
                        name: "FK_UploadHouses_OwnerAccounts_OwnerAccountID",
                        column: x => x.OwnerAccountID,
                        principalTable: "OwnerAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HouseSearches",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    HouseType = table.Column<string>(nullable: true),
                    AreaType = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    Zip = table.Column<int>(nullable: false),
                    Dates = table.Column<int>(nullable: false),
                    Radius = table.Column<int>(nullable: false),
                    Transportation = table.Column<string>(nullable: true),
                    PaymentType = table.Column<string>(nullable: true),
                    HavePlants = table.Column<string>(nullable: true),
                    Plants = table.Column<string>(nullable: true),
                    NumberOfPlants = table.Column<int>(nullable: false),
                    HavePets = table.Column<string>(nullable: true),
                    Pets = table.Column<string>(nullable: true),
                    NumberOfPets = table.Column<int>(nullable: false),
                    SitterAccountID = table.Column<int>(nullable: true),
                    UploadHouseID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HouseSearches", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HouseSearches_SitterAccounts_SitterAccountID",
                        column: x => x.SitterAccountID,
                        principalTable: "SitterAccounts",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HouseSearches_UploadHouses_UploadHouseID",
                        column: x => x.UploadHouseID,
                        principalTable: "UploadHouses",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_HouseSearches_SitterAccountID",
                table: "HouseSearches",
                column: "SitterAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_HouseSearches_UploadHouseID",
                table: "HouseSearches",
                column: "UploadHouseID");

            migrationBuilder.CreateIndex(
                name: "IX_OwnerAccounts_AssignedToId",
                table: "OwnerAccounts",
                column: "AssignedToId");

            migrationBuilder.CreateIndex(
                name: "IX_SitterAccounts_AssignedToId",
                table: "SitterAccounts",
                column: "AssignedToId");

            migrationBuilder.CreateIndex(
                name: "IX_UploadHouses_OwnerAccountID",
                table: "UploadHouses",
                column: "OwnerAccountID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HouseSearches");

            migrationBuilder.DropTable(
                name: "SitterAccounts");

            migrationBuilder.DropTable(
                name: "UploadHouses");

            migrationBuilder.DropTable(
                name: "OwnerAccounts");

            migrationBuilder.DropColumn(
                name: "BirthDate",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "DeletedAt",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Password",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "AspNetUsers");
        }
    }
}
