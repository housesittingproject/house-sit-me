﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace house_sit_me_website.Data
{
    public class SitterAccount
    {
        public int ID { get; set; } 
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        public DateTime BirthDate { get; set; }
        public string PasswordConfirmed { get; set; }
        public string EmailConfirmed { get; set; }
        public string CriminalBackgroundAgreement { get; set; }
        public ApplicationUser AssignedTo { get; set; }
        public virtual ICollection<HouseSearch> HouseSearches { get; } = new List<HouseSearch>();
        public virtual ICollection<OwnerAccount> OwnerAccounts { get; } = new List<OwnerAccount>();

    }
}
