﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using house_sit_me_website.Data;
using Microsoft.AspNetCore.Identity;

namespace house_sit_me_website.Pages.SitterCRUD
{
    public class CreateModel : PageModel
    {
        private readonly house_sit_me_website.Data.ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public CreateModel(house_sit_me_website.Data.ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public SitterAccount SitterAccount { get; set; }

        [BindProperty]
        public string CriminalBackgroundAgreement { get; set; }

        public List<string> Errors = new List<string>();

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            //Confirming email automatically to get around other redirects
            //Code mostly taken from RegisterSitter.cshtml.cs
            var user = new ApplicationUser { UserName = SitterAccount.Email, Email = SitterAccount.Email, EmailConfirmed = true };
            var result = await _userManager.CreateAsync(user, SitterAccount.Password);
            //TODO: REMOVE PLAINTEXT PASSWORD IN SITTERACCOUNT TABLE.
            //TODO: BE SURE TO USE PASSWORD TYPE INPUT FIELD

            if (result.Succeeded)
            {
                //_logger.LogInformation("User created a new account with password.");

                //var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                //var callbackUrl = Url.Page(
                //    "/Account/ConfirmEmail",
                //    pageHandler: null,
                //    values: new { userId = user.Id, code = code },
                //    protocol: Request.Scheme);

                //await _emailSender.SendEmailAsync(Input.Email, "Confirm your email",
                //    $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

                //await _signInManager.SignInAsync(user, isPersistent: false);
                //return LocalRedirect(returnUrl);
            }
            foreach (var error in result.Errors)
            {
                Errors.Add(error.Description);
                //ModelState.AddModelError(string.Empty, error.Description);
                return Page();
            }

            SitterAccount.AssignedTo = user;
            SitterAccount.CriminalBackgroundAgreement = CriminalBackgroundAgreement;

            _context.SitterAccounts.Add(SitterAccount);
            await _context.SaveChangesAsync();

            return RedirectToPage("/SitterCreateWorked");
        }
    }
}