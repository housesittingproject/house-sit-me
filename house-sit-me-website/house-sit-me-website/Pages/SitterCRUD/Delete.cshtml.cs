﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using house_sit_me_website.Data;

namespace house_sit_me_website.Pages.SitterCRUD
{
    public class DeleteModel : PageModel
    {
        private readonly house_sit_me_website.Data.ApplicationDbContext _context;

        public DeleteModel(house_sit_me_website.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public SitterAccount SitterAccount { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            SitterAccount = await _context.SitterAccounts.FirstOrDefaultAsync(m => m.ID == id);

            if (SitterAccount == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            SitterAccount = await _context.SitterAccounts.FindAsync(id);

            if (SitterAccount != null)
            {
                _context.SitterAccounts.Remove(SitterAccount);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
