﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using house_sit_me_website.Data;

namespace house_sit_me_website.Pages.UploadCRUD
{
    public class DeleteModel : PageModel
    {
        private readonly house_sit_me_website.Data.ApplicationDbContext _context;

        public DeleteModel(house_sit_me_website.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public UploadHouse UploadHouse { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            UploadHouse = await _context.UploadHouses.FirstOrDefaultAsync(m => m.ID == id);

            if (UploadHouse == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            UploadHouse = await _context.UploadHouses.FindAsync(id);

            if (UploadHouse != null)
            {
                _context.UploadHouses.Remove(UploadHouse);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
