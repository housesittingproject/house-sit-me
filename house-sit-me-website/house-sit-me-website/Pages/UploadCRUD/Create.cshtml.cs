﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using house_sit_me_website.Data;
using Microsoft.EntityFrameworkCore;

namespace house_sit_me_website.Pages.UploadCRUD
{

    public class CreateModel : PageModel
    {
        private readonly house_sit_me_website.Data.ApplicationDbContext _context;

        public CreateModel(house_sit_me_website.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public UploadHouse UploadHouse { get; set; }
        [BindProperty]
        public string HouseType { get; set; }
        [BindProperty]
        public string AreaType { get; set; }
        [BindProperty]
        public string Country { get; set; }
        [BindProperty]
        public string Transportation { get; set; }
        [BindProperty]
        public string PaymentType { get; set; }
        [BindProperty]
        public string HavePets { get; set; }
        [BindProperty]
        public string HavePlants { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            UploadHouse.HouseType = HouseType;
            UploadHouse.AreaType = AreaType;
            UploadHouse.Country = Country;
            UploadHouse.Transportation = Transportation;
            UploadHouse.PaymentType = PaymentType;
            UploadHouse.HavePets = HavePets;
            UploadHouse.HavePlants = HavePlants;

            _context.UploadHouses.Add(UploadHouse);
            await _context.SaveChangesAsync();

            return RedirectToPage("/NewRegisterWorked");
        }
    }
}