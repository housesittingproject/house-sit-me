﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using house_sit_me_website.Data;

namespace house_sit_me_website.Pages.HouseCRUD
{
    public class EditModel : PageModel
    {
        private readonly house_sit_me_website.Data.ApplicationDbContext _context;

        public EditModel(house_sit_me_website.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public HouseSearch HouseSearch { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            HouseSearch = await _context.HouseSearches.FirstOrDefaultAsync(m => m.ID == id);

            if (HouseSearch == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(HouseSearch).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HouseSearchExists(HouseSearch.ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool HouseSearchExists(int id)
        {
            return _context.HouseSearches.Any(e => e.ID == id);
        }
    }
}
