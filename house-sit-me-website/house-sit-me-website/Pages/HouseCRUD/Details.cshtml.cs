﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using house_sit_me_website.Data;

namespace house_sit_me_website.Pages.HouseCRUD
{
    public class DetailsModel : PageModel
    {
        private readonly house_sit_me_website.Data.ApplicationDbContext _context;

        public DetailsModel(house_sit_me_website.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public HouseSearch HouseSearch { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            HouseSearch = await _context.HouseSearches.FirstOrDefaultAsync(m => m.ID == id);

            if (HouseSearch == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
