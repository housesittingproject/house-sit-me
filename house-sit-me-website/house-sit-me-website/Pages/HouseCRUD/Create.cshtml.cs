﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using house_sit_me_website.Data;

namespace house_sit_me_website.Pages.HouseCRUD
{
    public class CreateModel : PageModel
    {
        private readonly house_sit_me_website.Data.ApplicationDbContext _context;

        public CreateModel(house_sit_me_website.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public HouseSearch HouseSearch { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.HouseSearches.Add(HouseSearch);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}