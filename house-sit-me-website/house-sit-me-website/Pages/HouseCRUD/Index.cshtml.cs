﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using house_sit_me_website.Data;

namespace house_sit_me_website.Pages.HouseCRUD
{
    public class IndexModel : PageModel
    {
        private readonly house_sit_me_website.Data.ApplicationDbContext _context;

        public IndexModel(house_sit_me_website.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<HouseSearch> HouseSearch { get;set; }

        public async Task OnGetAsync()
        {
            HouseSearch = await _context.HouseSearches.ToListAsync();
        }
    }
}
