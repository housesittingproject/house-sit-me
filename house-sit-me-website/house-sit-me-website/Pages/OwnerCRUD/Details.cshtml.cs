﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using house_sit_me_website.Data;

namespace house_sit_me_website.Pages.OwnerCRUD
{
    public class DetailsModel : PageModel
    {
        private readonly house_sit_me_website.Data.ApplicationDbContext _context;

        public DetailsModel(house_sit_me_website.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public OwnerAccount OwnerAccount { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            OwnerAccount = await _context.OwnerAccounts.FirstOrDefaultAsync(m => m.ID == id);

            if (OwnerAccount == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
