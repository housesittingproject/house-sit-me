﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using house_sit_me_website.Data;

namespace house_sit_me_website.Pages.OwnerCRUD
{
    public class EditModel : PageModel
    {
        private readonly house_sit_me_website.Data.ApplicationDbContext _context;

        public EditModel(house_sit_me_website.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public OwnerAccount OwnerAccount { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            OwnerAccount = await _context.OwnerAccounts.FirstOrDefaultAsync(m => m.ID == id);

            if (OwnerAccount == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(OwnerAccount).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OwnerAccountExists(OwnerAccount.ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool OwnerAccountExists(int id)
        {
            return _context.OwnerAccounts.Any(e => e.ID == id);
        }
    }
}
