using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using house_sit_me_website.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace house_sit_me_website.Pages
{
    public class HouseDetailsModel : PageModel
    {
        private readonly house_sit_me_website.Data.ApplicationDbContext _context;

        public HouseDetailsModel(house_sit_me_website.Data.ApplicationDbContext context)
        {
            _context = context;
        }
        public UploadHouse UploadHouse { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            UploadHouse = await _context.UploadHouses.FirstOrDefaultAsync(m => m.ID == id);

            if (UploadHouse == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}